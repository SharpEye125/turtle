import Tkinter as tk
from creeperawwman import *
from star import *
from suffering import *
from brodyproject import *
from daisyhtraining3 import *
from randomproject import *

class MainMenu:
    def __init__(self, master,*args,**kwargs):
        self.master = master
        self.master.minsize(700, 700)
        self.master.wm_title(". . . MENU . . .")
        self.master.option_add("*Font", "helvetica")
        # start frame
        self.frame = tk.Frame(self.master, relief='raised', borderwidth=1, background="#bbbbdd")
        self.frame.place(x=100, y=100,                    )
        self.button1 = tk.Button(self.frame, text = 'Aww Man', width = 25, command = awwMan)
        
        self.button1.pack()
        self.button2 = tk.Button(self.frame, text = 'Star', width = 25, command = star)
    
        self.button2.pack()
        self.button3 = tk.Button(self.frame, text = 'Suffering', width = 25, command = suffering)
        self.button3.pack()
        self.button4 = tk.Button(self.frame, text = 'Brody Project', width = 25, command = brodyproj)
        self.button4.pack()
        self.button5 = tk.Button(self.frame, text = 'Daisy\'s Training', width = 25, command = daisyhtraining)
        self.button5.pack()
        self.button5 = tk.Button(self.frame, text = 'Penta-Hexagon', width = 25, command = pentahexagon)
        self.button5.pack()
        self.quitButton = tk.Button(self.frame, text = 'Quit', width = 25, command = self.close_windows)
        self.quitButton.pack()
        self.text = tk.Label(self.frame, text="* * * * * * * * * * * * * \n \
Aww Man \n  \
* * * * * * * * * * * * * \n  \
Pink Star \n \
(Just a cool pink star) \n \
* * * * * * * * * * * * * \n \
suffering \n \
{true suffering) \n \
* * * * * * * * * * * * * \n \
brodyproject \n \
(cool looking mulit-level star) \n \
* * * * * * * * * * * * * \n \
Endless Circles \n \
Study of circles and color.\n \
* * * * * * * * * * * * * \n \
The 5 Hexagons \n \
Spider Man!!! \n \
* * * * * * * * * * * * * \n \
_________",font=('courier', '10'),background="#bbbbdd" )
        self.text.pack()
        self.frame.pack(expand=True, fill='both')
        # labels can be text or images
        # * * * * * * * * *

    def new_window(self):
        self.newWindow = tk.Toplevel(self.master)
        self.app = ChoasWindow(self.newWindow)
    def close_windows(self):
        self.master.destroy()

class ChoasWindow:
    def __init__(self, master):
        self.master = master
        self.frame = tk.Frame(self.master)
        self.quitButton = tk.Button(self.frame, text = 'Quit', width = 25, command = self.close_windows)
        self.quitButton.pack()
        self.frame.pack()
    def close_windows(self):
        self.master.destroy()

def main():
    root = tk.Tk()
    import sys
    print(sys.version)
    app = MainMenu(root)
    root.mainloop()

if __name__ == '__main__':
    main()


"""
apt install python3-pil python-pil python3-pil.imagetk python-pil.imagetk;
apt install python3-pil python-pil
apt install python3-pil.imagetk
apt install python-pil.imagetk

https://pythonprogramming.net/tkinter-adding-text-images/
https://stackoverflow.com/questions/17466561/best-way-to-structure-a-tkinter-application
https://stackoverflow.com/questions/23901168/how-do-i-insert-a-jpeg-image-into-a-python-tkinter-window
$base03:    #002b36; (dark grey)
$base02:    #073642;
$base01:    #586e75;
$base00:    #657b83;
$base0:     #839496;
$base1:     #93a1a1;
$base2:     #eee8d5; (cream)
$base3:     #fdf6e3;
$yellow:    #b58900;
$orange:    #cb4b16;
$red:       #dc322f;
$magenta:   #d33682;
$violet:    #6c71c4;
$blue:      #268bd2;
$cyan:      #2aa198;
$green:     #859900;

"""
