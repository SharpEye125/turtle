#import tkinker as tk
import turtle
def pentahexagon():
	colors = [ "purple","dark blue","white","red"]
	sketch = turtle.Turtle()
	turtle.bgcolor("black")
	sketch.speed(1200)
	for i in range(200):
		sketch.pencolor(colors[i % 4])
		sketch.width(i/100 + 1)
		sketch.forward(i)
		sketch.left(59)
		sketch.right(29)
		sketch.left(30)
	sketch.goto(100,100)
	for i in range(100): 
		sketch.pencolor(colors[i % 4])
		sketch.width(i/100 + 1)
		sketch.forward(i)
		sketch.left(59)
		sketch.right(29)
		sketch.left(30)
	sketch.goto(-100,100)
	for i in range(100): 
		sketch.pencolor(colors[i % 4])
		sketch.width(i/100 + 1)
		sketch.forward(i)
		sketch.left(59)
		sketch.right(29)
		sketch.left(30)
	sketch.goto(100,-100)
	for i in range(100): 
		sketch.pencolor(colors[i % 4])
		sketch.width(i/100 + 1)
		sketch.forward(i)
		sketch.left(59)
		sketch.right(29)
		sketch.left(30)
	sketch.goto(-100,-100)
	for i in range(100): 
		sketch.pencolor(colors[i % 4])
		sketch.width(i/100 + 1)
		sketch.forward(i)
		sketch.left(59)
		sketch.right(29)
		sketch.left(30)
	sketch.exitonclick()
#pentahexagon()

